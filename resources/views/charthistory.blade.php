<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <title>Laravel RealTime Using Google Firebase</title>

</head>
<body>

<div class="container" style="margin-top: 50px;">

    <h1 class="text-center" style="font-weight: bold; font-size:60px;">Senja</h1><br>
    <h3 class="text-center">Warung Jajan Veteran</h3>
    <p class="text-center">Jalan Veteran & Bintaro, Jakarta Selatan</p>

    <br>

    <h5># Chart History</h5>
    <table class="table table-bordered">
        <tr>
            <th>Tanggal Pesan</th>
            <th>Pesanan</th>
            <th>Jumlah</th>
            <th>Total Harga</th>
        </tr>
        <tbody id="tbody">

        </tbody>
    </table>
</div>

{{--Firebase Tasks--}}
<script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.10.1/firebase.js"></script>
<script>
    // Initialize Firebase
    var config = {
        apiKey: "{{ config('services.firebase.api_key') }}",
        authDomain: "{{ config('services.firebase.auth_domain') }}",
        databaseURL: "{{ config('services.firebase.database_url') }}",
        storageBucket: "{{ config('services.firebase.storage_bucket') }}",
    };
    firebase.initializeApp(config);

    var database = firebase.database();

    var lastIndex = 0;

    // Get Data
    firebase.database().ref('Cart_History/').on('value', function (snapshot) {
        var value = snapshot.val();
        var htmls = [];
        $.each(value, function (index, value) {
            if (value) {
                htmls.push('<tr>\
        		<td>' + value.Tgl_pesan + '</td>\
        		<td>' + value.nama_menu + '</td>\
        		<td>' + value.jumlah + '</td>\
        		<td>' + value.Total_Harga + '</td>\
        	</tr>');
            }
            lastIndex = index;
        });
        $('#tbody').html(htmls);
        $("#submitUser").removeClass('desabled');
    });
</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>
</html>